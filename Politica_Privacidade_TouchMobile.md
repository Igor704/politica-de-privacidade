TERMOS DE USO E POLÍTICA DE PRIVACIDADE

TOUCH COMP SISTEMAS LTDA., pessoa jurídica de direito privado, inscrita no CNPJ sob o nº 17.254.557/0001-51, sediada na Rua Ovídio Silva, 178, Sala 201, Bairro Nogueira Machado, Itaúna, MG, CEP 35680-237, vem, pelo presente, sobretudo de forma a demonstrar o seu compromisso com a privacidade e a segurança dos usuários de seu APP, apresentar os seus Termos de Uso e sua Política de Privacidade, conforme abaixo exposto:

1. Dicionário de definições:
 
Consentimento: manifestação livre, informada e inequívoca de uma pessoa (titular), quanto ao tratamento dos seus dados pessoais para as finalidades apresentadas neste documento e para a utilização do APP, ou seja, sua autorização;

Dados anônimos: dados relativos a uma pessoa que não pode ser identificada, considerando a utilização de meios técnicos razoáveis e disponíveis na ocasião de seu tratamento;

Dados pessoais: quaisquer dados que possam identificar uma pessoa;

Dados pessoais cadastrais: dados pessoais de interesse público que identificam e qualificam as pessoas e que não são sigilosos. Em se tratando de pessoas jurídicas, são exemplos a razão social, o CNPJ, o endereço, telefone(s), e-mail(s), o(s) nome(s) e dados do(s) respectivo(s) sócio(s) administrador(es), procurador(es) etc. Em se tratando de pessoas físicas, são exemplos o nome, RG, CPF, endereço, estado civil, profissão, data de nascimento, nome da mãe, telefone etc.;

Dispositivos: são computadores, notebooks, tablets, smartphones e quaisquer outros aparelhos utilizados para o acesso ao APP;

IP: sigla para "Internet Protocol". É o protocolo que identifica, localiza e estabelece conexão entre computadores e demais dispositivos ligados à internet;

Site: conjunto de informações disponibilizadas na internet por indivíduo, instituição, empresa etc., pertencente a um mesmo endereço eletrônico e que geralmente trata de tema específico;

Titulares: são os usuários do APP.

2. Dados coletados pela Touch Comp:

Dados pessoais cadastrais dos usuários do APP: nome completo, número do CPF, data de nascimento, endereço, e-mail e telefone. Somente terão acesso ao APP os usuários autorizados pelas empresas contratantes da Touch Comp, sendo de inteira responsabilidade das mesmas o repasse, a este última (Touch Comp), dos seus dados, sendo elas responsáveis, ainda, por firmar, por meio de documento escrito, o consentimento dos respectivos titulares, para esse fim.

Dados necessários à utilização das soluções oferecidas pela Touch Comp: são todos aqueles dados necessários à utilização do APP. É de inteira responsabilidade, das empresas contratantes da Touch Comp, o repasse desses dados a mesma, sendo de inteira responsabilidade delas, ainda, firmar, por meio de documento escrito, o consentimento dos respectivos titulares para esse fim, acaso se faça necessário;

3. Finalidade dos dados coletados pela Touch Comp:

Todos os dados coletados, pela Touch Comp, de destinam à utilização do APP.

Portanto, os seus usuários manifestam sua expressa e irrestrita ciência e concordância com a coleta – inclusive por meio das respectivas câmeras e GPS’s dos aparelhos mediante os quais o APP será acessado –, utilização, armazenamento e tratamento desses dados, o que ocorrerá por todo o período pelo qual houver a sua utilização.

A Touch Comp não interferirá em nenhum processo que envolva os objetos sociais das empresas que a contrataram, ou seja, fica, sob a única e exclusiva responsabilidade das mesmas a celebração dos negócios jurídicos pertinentes as suas atividades. 

4. Hipóteses aplicáveis para o tratamento de dados pessoais:

A ﬁm de garantir a conformidade com a Lei Geral de Proteção de Dados (LGPD), há de ser ressaltado que toda operação realizada com quaisquer dados pessoais está estritamente ligada aos contratos firmados entre a Touch Comp e as empresas clientes, se destinando, exclusivamente, à consecução de seu objeto.

5. Da disponibilização dos dados pessoais coletados pela Touch Comp:

A Touch Comp trata, tão somente, dados pessoais que entende serem os mínimos necessários para a consecução do objeto do contrato firmado com suas empresas clientes, os disponibilizando apenas para elas próprias e, eventualmente, para entidades, órgãos etc. aos quais esteja obrigada, por motivos legais.

A Touch Comp também pode disponibilizar os dados pessoais, quando estritamente necessário, a agentes envolvidos na própria prestação dos seus serviços.

Portanto, os usuários do APP manifestam sua expressa e irrestrita ciência e concordância com a disponibilização dos dados, nos moldes acima informados, o que ocorrerá por todo o período pelo qual houver a sua utilização.

6. Requisitos para a utilização do APP:

O APP poderá ser acessado mediante a observância de todo o exposto neste instrumento.

A Touch Comp esclarece, inclusive, que, para tanto, realiza processo de confirmação de acesso dos usuários ao APP por meio de logins e senhas pessoais e de caráter individual e intransferível.

As empresas contratantes da Touch Comp, assim como os usuários do APP, são os responsáveis pela exatidão, veracidade e atualização dos dados pessoais informados, concordando que eventuais divergências e/ou inconsistências desses dados poderão ocasionar a não validação da sua identidade, o que poderá impedir o acesso a todas ou a algumas funcionalidades do mesmo, como medida de segurança.

7. Responsabilidade pelo armazenamento dos dados pessoais:

Os dados pessoais repassados, à Touch Comp, de forma a possibilitar o acesso dos usuários ao APP, serão armazenados em seus sistemas, de forma criptografada e, portanto, totalmente segura e sigilosa, sendo que os demais dados inseridos no mesmo serão armazenados no próprio banco de dados das empresas contratantes da Touch Comp.
 
8. Direitos sobre o tratamento dos dados pessoais:

Os usuários do APP podem solicitar, sempre que desejarem, acesso aos dados pessoais repassados à Touch Comp, podendo solicitar, ainda, a sua atualização, correção e exclusão.

Para tanto, deve ser apresentado pedido expresso, a ser entregue na sede da Touch Comp, pessoalmente ou por meio postal, ou, do contrário, por meio de ferramenta específica, tão logo seja ela disponibilizada no próprio APP.

Importante se faz ser ressaltado que alguns pedidos de correção, alteração ou exclusão de dados, de acordo com o modo de funcionamento do APP, não poderão ser atendidos, sendo que, nessa hipótese, serão sempre prestadas informações sobre o por quê deste fato.

De igual forma, poderão ser revogados os consentimentos dos respectivos titulares dos dados, sendo necessário ser destacado, novamente, que, em relação aos mesmos, toda a responsabilidade de formalização dos documentos escritos recai sobre as empresas contratantes da Touch Comp, que, por esta razão, ficam impedidas de recusá-los, devendo os encaminhar, de imediato, a esta última (Touch Comp).

9. Responsabilidade pelo usuário e senha:

A Touch Comp esclarece, mais uma vez, que, para acesso ao APP, realiza processo de confirmação dos usuários por meio de logins e senhas pessoais e de caráter individual e intransferível.

10. Compromisso antispam e antiphishing:

A Touch Comp sempre atua de acordo com as boas práticas do mercado digital, razão pela qual recomenda que, na hipótese dos usuários receberem qualquer e-mail em seu nome e suspeitarem de fraude, não abram eventuais arquivos anexos e nem cliquem em qualquer link ou botão, a contatando imediatamente, por meio de seus canais de atendimento, para que averigue o ocorrido e tome as medidas cabíveis para o combate ao crime eletrônico.

11. Relacionamento com terceiros:

A Touch Comp, ao contratar outras empresas, parceiros etc. para prestarem serviços, exige dos mesmos garantias de privacidade e segurança, compatíveis com as asseguradas neste instrumento. 

12. Segurança de dados:

A Touch Comp conta com os mais modernos e eficientes meios de proteção aos dados pessoais acessados por meio de seu APP, utilizando protocolos de segurança de ponta, que estão em constante atualização, sempre visando assegurar a integridade e a confidencialidade dos mesmos.

13. Direitos autorais:

Todos os textos, imagens, sons etc. exibidos no APP da Touch Comp são protegidos por direitos autorais, não sendo permitido modifica-los, reproduzi-los, armazena-los, transmiti-los, copia-los, distribui-los ou utiliza-los para outros fins que não estejam estritamente ligados à utilização do mesmo (APP).

14. Alterações deste instrumento:
 
Este instrumento está sujeito a alterações a qualquer momento. Toda e qualquer alteração visa se adequar às eventuais modificações em nosso APP, sejam relativas a mudanças para novas tecnologias e/ou sempre que se fizer necessário. Visam, de igual forma, se adequar a novos requisitos legais, regulatórios ou contratuais.
 
Quaisquer alterações serão devidamente comunicadas por meio de envio de e-mail aos endereços eletrônicos dos usuários e/ou por meio de acesso ao próprio APP.